function closeMobileMenu() {
    jQuery(".navbar-collapse").collapse("hide");
    console.log("close mobile navbar");
}

function doSearch() {
    if (typeof doAjaxSearch === "function") {
        doAjaxSearch();
    }
    else {
        console.log("doAjaxSearch() is not defined");
    }
    if ( jQuery("#search-catalog-xl").length ) {
        jQuery(".navbar-collapse").collapse("hide");
        jQuery(document).scrollTop( $("#search-container").offset().top );
        jQuery(".hamburger").removeClass("is-active");
    }
    else {
        console.log("no need to close menu and scroll: xl media");
    }
}

function overrideSearchForm() {
    if ( jQuery("#search-catalog-xl").is(":hidden") ) {
        console.log("#search-catalog-xl is hidden, set #search-catalog to #search-catalog-sm");
        jQuery("#search-catalog-sm").attr("id", "search-catalog");
    }
    else {
        console.log("#search-catalog-xl is visible, set #search-catalog");
        jQuery("#search-catalog-xl").attr("id", "search-catalog");
    }
    if ( jQuery("#search-container").length ) {
        console.log("no need to override search container");
    }
    else {
        jQuery("#generic-container").attr("id", "search-container");
    }
}

overrideSearchForm();
